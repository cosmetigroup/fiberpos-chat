import * as messageStatusTypes from '../store/message-status-types.js';
import * as types from '../store/mutation-types';
import * as BLOB_IMAGES from "../assets/js/utility/blob-images";

export default {
    methods: {
        isThreadTypeDirect(thread) {
            return thread.thread_type === 1;
        },
        isThreadTypeGroup(thread) {
            return thread.thread_type > 1;
        },
        isHistoryLogOthers(historyLog) {
            return historyLog.sender === 'others';
        },
        isHistoryLogSelf(historyLog) {
            return historyLog.sender === 'self';
        },
        isHistoryLogStatusSeen(historyLog) {
            return historyLog.status === messageStatusTypes.SEEN;
        },
        isHistoryLogStatusSeenAll(historyLog) {
            return historyLog.status === messageStatusTypes.SEEN_BY_ALL;
        },
        isHistoryLogStatusSent(historyLog) {
            return historyLog.status === messageStatusTypes.SENT;
        },
        isHistoryLogStatusSending(historyLog) {
            return historyLog.status === messageStatusTypes.SENDING;
        },
        isHistoryLogStatusRetry(historyLog) {
            return historyLog.status === messageStatusTypes.FAILED_TO_SEND;
        },
        closeChatbox() {
            this.$root.$emit('on::chatbox::close');
        },
        scrollToBottom(el) {
            if (el !== undefined) {
                el.scrollTop = el.scrollHeight;
            }
        },
        showToastSuccess(title, message) {
            if (common !== undefined) {
                common.showToastSuccess(title, message);
                return;
            }
            // Toaster Fallback
            alert(`${title}: ${message}`)
        },
        showToastError(title, message) {
            if (common !== undefined) {
                common.showToastError(title, message);
                return;
            }
            // Toaster Fallback
            alert(`${title}: ${message}`)
        },
        setDisconnectedStatus(status) {
            this.$store.commit(types.SET_DISCONNECTED_STATUS, status);
        },
        checkInternetConnection(erpUrl) {
            return new Promise((resolve) => {
                if (!erpUrl) {
                    resolve(false);
                    return;
                }

                fetch(erpUrl, {cache: 'no-cache'}).then((response) => {
                    resolve(response.ok)
                }).catch(() => {
                    resolve(false)
                })
            });
        },
        getImageUrl(imgUrl) {
            if (imgUrl === '/bundles/gisttemplate/assets/global/img/logo-only-transparent.png') {
                return BLOB_IMAGES.DEFAULT_GROUP_AVATAR;
            }
            if (
                imgUrl === '/bundles/gisttemplate/assets/admin/layout/img/user_avatar.jpg' ||
                imgUrl === '/build/images/user_avatar.jpg'
            ) {
                return BLOB_IMAGES.DEFAULT_USER_AVATAR;
            }
            if (!imgUrl) {
                return BLOB_IMAGES.DEFAULT_NO_IMAGE;
            }
            return imgUrl;
        }
    }
}