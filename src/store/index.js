import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import mutations from './mutations'
import createLogger from '../assets/plugins/logger';


Vue.use(Vuex);

const state = {
    canAddGroups: false,
    canManageGroups: false,
    loggedUserId: null,
    loggedUserPhoto: null,
    totalUnreadMessagesCount: 0,
    isDisconnected: false,
    isChatOpen: false,
    isLoadingHistory: false,
    showMessageSection: false,
    currentThreadKey: null,
    currentThreadID: null,
    typingTimeout: 900,
    participants: [],
    onlineUsers: [],
    userObject: {},
    threads: [],
    messages: [],
    members: [],
    membersSelected: [],
    gcvName: null,
    gccName: null,
    gccSearchFilter: null,
    gccUsers: [],
    gccSelectedUserIDs: [],
    newUserIDs: [],
    isFromPOS: false,
};

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
    plugins: process.env.NODE_ENV !== 'production'
        ? [createLogger()]
        : []
});
