export const TEXT = 1;
export const FILE = 2;
export const IMAGE = 3;
export const DATE_SEPARATOR = 4;
export const SYSTEM_GENERATED = 5;