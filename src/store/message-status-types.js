export const SENDING = 1;
export const SENT = 2;
export const SEEN = 3;
export const SEEN_BY_ALL = 4;
export const FAILED_TO_SEND = 5;
