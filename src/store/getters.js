export const isDisconnected = state => state.isDisconnected;
export const isChatOpen = state => state.isChatOpen;
export const loggedUserId = state => state.loggedUserId;
export const showMessagesSection = state => state.showMessageSection;
export const currentThreadID = state => state.currentThreadID;
export const currentThreadKey = state => state.currentThreadKey;
export const threads = state => state.threads;
export const messages = state => state.messages;
export const members = state => state.members;
export const gccUsers = state => state.gccUsers;
export const groupChatName = state => state.gccName;
export const onlineUsers = state => state.onlineUsers;
export const gccSelectedUserIDs = state => state.gccSelectedUserIDs;
export const typingTimeout = state => state.typingTimeout;
export const canAddGroups = state => state.canAddGroups;
export const canManageGroups = state => state.canManageGroups;
export const participants = state => state.participants;
export const totalUnreadMessagesCount = state => state.totalUnreadMessagesCount;
export const loggedUserProfilePhoto = state => state.loggedUserPhoto;
export const isLoadingHistory = state => state.isLoadingHistory;
export const isFromPOS = state => state.isFromPOS;
export const membersSelected = state => {
    if (state.membersSelected.length === 0) {
        return [];
    }

    return state.membersSelected;
};
export const currentThread = state => {
    return state.currentThreadID ? state.threads.find(function (thread) {
        return thread.id === state.currentThreadID;
    }) : {};
};

export const sortedMessages = state => {
    if (state.messages.length === 0) {
        return [];
    }

    return state.messages
        .slice()
        .sort((a, b) => a.date_timestamp - b.date_timestamp);
};

export const sortedThreads = state => {
    if (state.threads.length === 0) {
        return [];
    }

    return state.threads
        .slice()
        .sort((a, b) => b.date_timestamp - a.date_timestamp);
};

export const threadMessagesIDs = state => {
    return state.messages.map(message => message.id);
};

export const currentThreadMemberIDs = state => {
    if (state.currentThreadKey != null) {
        return state.threads[state.currentThreadKey].members.map(member => member.id);
    }
};