import Vue from 'vue'

Vue.filter('toCurrency', function (value) {
	var formatter = new Intl.NumberFormat('en-US', {
		style: 'decimal',
		minimumFractionDigits: 2
	});
	return formatter.format(value);
});

Vue.filter('toOrderNumber', function (value) {
	return value.slice(0, 3) + "-" + value.slice(3);
});
