import Vue from 'vue';
import moment from 'dayjs';

Vue.filter('formatDate', function (value) {
	return moment.unix(value).format("MM-DD-YYYY");
});

Vue.filter('formatTime', function (value) {
	return moment.unix(value).format("hh:mm A");
});

Vue.filter('formatDateTime', function (value) {
	return moment.unix(value).format("MM-DD-YYYY hh:mm A");
});

Vue.filter('formatToUnix', function (value) {
	return parseInt(moment(value).format("X"));
});

Vue.filter('formatDateInterval', (value) => {
	if (value) {
		return moment(String(value)).fromNow();
	}
});
