import {
    sendChatThreadMessage as apiSendChatThreadMessage
} from "../../../assets/api/resources";
import * as messageStatusTypes from '../../../store/message-status-types';
import * as messageTypes from '../../../store/message-types';

export function createMessage({text, thread, loggedUserId, profilePicture, messageType, file, blob}, cb) {
    const timestamp = Math.floor(Date.now() / 1000);
    const threadId = thread.id;
    const id = 'tmp_' + timestamp;
    let fileData = {};
    const message = {
        id,
        sender: 'self',
        message: text,
        img_url: profilePicture,
        date_timestamp: timestamp,
        status: messageStatusTypes.SENDING,
        seen_by: [],
        threadId: threadId,
        authorId: loggedUserId,
        messageType,
        file_url: blob
    };

    cb(message);

    if (messageType === messageTypes.FILE || messageType === messageTypes.IMAGE) {
        fileData = {
            threadId: threadId,
            from: "local",
            lastModified: file.lastModified,
            lastModifiedDate: file.lastModifiedDate,
            name: file.name,
            size: file.size,
            src: blob,
            type: file.type
        };
    }

    let data = {
        userId: loggedUserId,
        threadId: threadId,
        text: text,
        tempId: id,
        messageType,
        fileData
    };

    apiSendChatThreadMessage(data, ({responseData}) => {
        message.status = messageStatusTypes.SENT;
        message.id = responseData.id;
        message.date_timestamp = responseData.date_timestamp;
        message.file_url = responseData.file_url;
        cb(message);
    }, ({}) => {
        message.status = messageStatusTypes.FAILED_TO_SEND;
        cb(message);
    });
}
