import api from '../index'

let erp_url_meta = document.head.querySelector('meta[name="erp-url"]');
let erp_url = erp_url_meta ? erp_url_meta.content : '';

export function getChatThreads(data, successHandler, errorHandler = null) {
    // data = {
    //      userId: string,
    //      startPage: string,
    //      endPage: string,
    //      limit: string
    //      searchFilter: string
    // };
    api.post(`${erp_url}/chat/threads`, data, successHandler, errorHandler);
}

export function getChatThreadMessages(userId, threadId, startPage, endPage, limit, successHandler, errorHandler = null) {
    api.get(`${erp_url}/chat/thread-messages/${userId}/${threadId}/${startPage}/${endPage}/${limit}`, successHandler, errorHandler);
}

export function sendChatThreadMessage(data, successHandler, errorHandler = null) {
    // data = {
    //      userId: string,
    //      threadId: string,
    //      text: string,
    //      tempId: string
    // };
    api.post(`${erp_url}/chat/thread-messages/send`, data, successHandler, errorHandler);
}

export function setMessagesAsRead(data, successHandler, errorHandler = null) {
    // data = {
    //     threadId: string,
    //     messageIDs: array,
    //     userId: string,
    // };
    api.post(`${erp_url}/chat/thread-messages/mark-as-seen`, data, successHandler, errorHandler);
}

export function getUnreadMessagesCount(userId, successHandler, errorHandler = null) {
    api.get(`${erp_url}/chat/thread-messages/unread-count/${userId}`, successHandler, errorHandler);
}

export function createGroupChat(data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string,
    //     threadName: string,
    //     participants: array,
    //     imageFile: array
    // };
    api.post(`${erp_url}/chat/create-thread`, data, successHandler, errorHandler);
}

export function getUserOptionsForGroup(data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string,
    //     filter: string,
    // };
    api.post(`${erp_url}/chat/get-user-options`, data, successHandler, errorHandler);
}

export function removeUsersFromThread(data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string,
    //     threadId: string,
    //     threadParticipantIDs: string,
    // };
    api.post(`${erp_url}/chat/remove-users-from-thread`, data, successHandler, errorHandler);
}

export function getThreadUsers(data, successHandler, errorHandler = null) {
    // data = {
    //     threadId: string,
    //     filter: string,
    // };
    api.post(`${erp_url}/chat/get-thread-users`, data, successHandler, errorHandler);
}

export function updateGroupChat(data, successHandler, errorHandler = null) {
    // data = {
    //     threadId: string,
    //     newName: string,
    //     userId:: string,
    //     imageFile: array
    // };
    api.post(`${erp_url}/chat/update-thread`, data, successHandler, errorHandler);
}

export function broadcastStatus(data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string,
    //     status: string,
    // };
    api.post(`${erp_url}/chat/broadcast/status`, data, successHandler, errorHandler);
}

export function getOnlineUsers(data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string
    // };
    api.post(`${erp_url}/chat/online-users`, data, successHandler, errorHandler);
}

export function getShortcutSettingForm(path, successHandler, errorHandler = null) {
    api.get(path, successHandler, errorHandler);
}

export function submitShortcutSettingForm(path, data, successHandler, errorHandler = null) {
    api.post(path, data, successHandler, errorHandler);
}

export function addThreadUsers(data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string
    //     userIDs: array
    //     threadId: string
    // };
    api.post(`${erp_url}/chat/thread/add-users`, data, successHandler, errorHandler);
}

export function changeUserAvatar(data, successHandler, errorHandler = null) {
    // data = {
    //      userId: string,
    //      fileData: array
    // };
    api.post(`${erp_url}/chat/user/change-avatar`, data, successHandler, errorHandler);
}

export function syncUserAvatar(data, successHandler, errorHandler = null) {
    // data = {
    //      userId: string
    // };
    api.post(`/inventory/syncing/user_avatar_erp_to_pos`, data, successHandler, errorHandler);
}