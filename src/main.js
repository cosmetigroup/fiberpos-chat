import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import FiberposChat from "./components/ChatMain";
import store from './store';
import './assets/js/filters/filters-date';

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
	el: '#chat',
    components: {
        FiberposChat
    },
	store
});