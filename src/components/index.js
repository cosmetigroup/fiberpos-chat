import Vue from "vue";
import FiberposChat from "./ChatMain";

const Components = {
	FiberposChat
};

Object.keys(Components).forEach(name => {
	Vue.component(name, Components[name]);
});

export default Components;